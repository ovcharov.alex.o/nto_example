import sys
import os
import time

from nto_sim.py_nto_task1 import Task
import cv2

## Здесь должно работать ваше решение
def solve(task: Task):

    ## Загружаем изображение из задачи
    # сцена отправляется с определенной частотй
    # для этого смотри в документацию к задаче
    sceneImg = task.getTaskScene()
    mapImage = task.getTaskMap()

    taskInfo = task.getTask()
    print('Task: ', taskInfo)

    robotsSize = task.robotsSize()
    print('Number of robots:', robotsSize)
    print("Robot 0")
    state = task.getRobotState(0)
    print("Left Motor angle:", state.leftMotorAngle)
    print("Right Motor angle:", state.rightMotorAngle)
    print("Left Motor speed:", state.leftMotorSpeed)
    print("Right Motor speed:", state.rightMotorSpeed)


    # while (run):
    for i in range(10):

        v = [10, 5]
        task.setMotorVoltage(0, v)

        sceneImg = task.getTaskScene()
        #cv2.imshow('Scene', sceneImg)
        #cv2.waitKey(20) # мс
        print("Turn ", i)
        time.sleep(1)

if __name__ == '__main__':

    ## Запуск задания и таймера (внутри задания)
    task = Task()
    task.start()

    solve(task)

    task.stop()
